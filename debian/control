Source: precious
Section: devel
Priority: optional
Maintainer: Jonas Smedegaard <dr@jones.dk>
Build-Depends:
 cmark-gfm <!nodoc>,
 debhelper-compat (= 13),
 dh-sequence-rust,
 git <!nocheck>,
 help2man <!nodoc>,
 librust-anyhow-1+default-dev (>= 1.0.75),
 librust-clap-4+cargo-dev,
 librust-clap-4+default-dev,
 librust-clap-4+derive-dev,
 librust-clap-4+wrap-help-dev,
 librust-comfy-table-7+default-dev,
 librust-env-logger-0.11+default-dev,
 librust-fern+colored-dev (<< 7),
 librust-fern+default-dev (<< 7),
 librust-filetime-0.2+default-dev (>= 0.2.22),
 librust-globset-0.4+default-dev (>= 0.4.13),
 librust-ignore-0.4+default-dev (>= 0.4.20),
 librust-indexmap-2+default-dev,
 librust-indexmap-2+serde-dev,
 librust-itertools-0.13+default-dev,
 librust-log-0.4+default-dev (>= 0.4.20),
 librust-md5-0.7+default-dev,
 librust-once-cell-1+default-dev (>= 1.18),
 librust-pathdiff-0.2+default-dev,
 librust-pretty-assertions-1+default-dev (>= 1.4),
 librust-rayon-1+default-dev (>= 1.8),
 librust-regex-1+default-dev (>= 1.10.2),
 librust-serde-1+default-dev (>= 1.0.189),
 librust-serde-1+derive-dev (>= 1.0.189),
 librust-serial-test-dev (<< 4),
 librust-tempfile-3+default-dev (>= 3.8),
 librust-test-case-3+default-dev,
 librust-thiserror-1+default-dev (>= 1.0.49),
 librust-toml-0.8+default-dev,
 librust-which-6+default-dev,
 libstring-shellquote-perl <!nodoc>,
Standards-Version: 4.7.0
Homepage: https://github.com/houseabsolute/precious
Vcs-Git: https://salsa.debian.org/debian/precious.git
Vcs-Browser: https://salsa.debian.org/debian/precious
Rules-Requires-Root: no

Package: precious
Architecture: any
Depends:
 ${misc:Depends},
 ${shlibs:Depends},
Suggests:
 git,
 golang-golang-x-tools,
 libperl-critic-perl,
 perl,
 perltidy,
 rust-clippy,
 rustfmt,
Built-Using:
 ${cargo:Built-Using},
Static-Built-Using:
 ${cargo:Static-Built-Using},
Description: one code quality tool to rule them all
 Precious is a code quality tool
 that lets you run all of your linters and tidiers
 with a single command.
 .
 It's features include:
  * One file, `precious.toml`,
    defines all of your linter and tidier commands,
    as well as what files they operate on.
  * Respects VCS ignore files
    and allows global and per-command excludes.
  * Language-agnostic,
    and works the same way with single- or multi-language projects.
  * Easy integration with commit hooks and CI systems.
  * Commands are executed in parallel by default,
    with one process per CPU.
  * Commands can be grouped with labels,
    for example to just run a subset of commands for commit hooks
    and all commands in CI.
